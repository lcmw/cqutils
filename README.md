# README #

The CQUtils is a library for opening windows and accepting input within LWJGL 3. It uses version 3.0 but is pending an update to the mess that is the custom jar system.

### Setup ###

* Download the jar release.
* In eclipse open the build dependencies window of your project.
* Add the CQUtils.jar


## In order to use the CQUtil library you must have the following: ##
* The LWJGL 3.0 </b>single file jar</b> with it's natives set up.
* [PNGDecoder](https://drive.google.com/file/d/0B4_SgVGfVtFWMVlXSnkyeXIwdWs/view?usp=sharing) for loading icons.
* [JOML](https://github.com/JOML-CI/JOML) because I'm lazy and it's in the libraries so fuck you.

* See the example package for how to open a display and other fun stuff.
### Contact ###
My twitter is [LCMW_Spud](https://twitter.com/SpudException) feel free to contact me over that.