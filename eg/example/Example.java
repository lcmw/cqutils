package example;

import org.lwjgl.glfw.GLFW;

import com.lcmw.cqe.util.CQEngine;
import com.lcmw.cqe.util.CQMainApp;
import com.lcmw.cqe.util.Display;
import com.lcmw.cqe.util.DisplayConfig;
import com.lcmw.cqe.util.Input;
import com.lcmw.cqe.util.Mouse;

/**
 * Creates a Display and prints input using the CQToolkit
 * */
public class Example implements CQMainApp {
	
	public static void main(String args[]) {
		DisplayConfig cfg = new DisplayConfig(1280, 720, "This is a title");
		Display.create(cfg);
		
		CQEngine.begin(new Example());
	}

	@Override
	public void load() {	
		System.out.println("Initialisation");
	}

	@Override
	public void render() {		
		System.out.println("Render");
	}

	@Override
	public void update(float dt) {
		if(Input.isKeyDown(GLFW.GLFW_KEY_A))
			System.out.println("A is down");
		if(Input.isKeyPressed(GLFW.GLFW_KEY_S))
				System.out.println("S is pressed");
	
		if(Mouse.isButtonClicked(GLFW.GLFW_MOUSE_BUTTON_LEFT))
			System.out.println("Mouse button left clicked at: (" + Mouse.getX() + ", " + Mouse.getY() + ")");
		if(Mouse.isButtonDown(GLFW.GLFW_MOUSE_BUTTON_RIGHT))
			System.out.println("Mouse button right down at: (" + Mouse.getX() + ", " + Mouse.getY() + ")");
	}

	@Override
	public void exit() {
		System.out.println("Exit");
	}
	
}
