package com.lcmw.cqe.util;

/**
 * TODO: NOT FINISHED
 * */
public class DisplayConfig {

	private int width, height, samples = 4, fps = 60;
	private boolean fullscreen = false, resizable = false, border = true;
	private String title;
	//ICON
	
	public DisplayConfig(int width, int height, String title) {
		this.width = width;
		this.height = height;
		this.title = title;
	}
	
	public int getWidth() {
		return width;
	}
	public boolean isBordered() {
		return border;
	}
	public void setBordered(boolean border) {
		this.border = border;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public boolean isFullscreen() {
		return fullscreen;
	}
	public void setFullscreen(boolean fullscreen) {
		this.fullscreen = fullscreen;
	}
	public boolean isResizable() {
		return resizable;
	}
	public void setResizable(boolean resizable) {
		this.resizable = resizable;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getSamples() {
		return samples;
	}
	public void setSamples(int samples) {
		this.samples = samples;
	}
	public boolean isBorder() {
		return border;
	}
	public int getDesieredFps() {
		return fps;
	}
	public void setDesieredFps(int fps) {
		this.fps = fps;
	}
}
