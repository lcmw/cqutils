package com.lcmw.cqe.util;

import java.util.ArrayList;

public abstract class Delete {
	private static ArrayList<Delete> deletes = new ArrayList<>();
	
	public Delete() {
		deletes.add(this);
	}
	
	public abstract void delete();
	
	public static void deleteAll() {
		for(Delete del:deletes) {
			del.delete();
		}
		
		deletes.clear();
	}
}
