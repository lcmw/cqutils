package com.lcmw.cqe.util;

public interface CQMainApp {
	public void load();
	public void render();
	public void update(float dt);
	public void exit();
}
