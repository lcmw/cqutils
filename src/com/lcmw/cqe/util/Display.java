package com.lcmw.cqe.util;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

public class Display {

	private static long window;
	private static DisplayConfig cfg;
	
	public static void create(DisplayConfig cfg) {
		Display.cfg = cfg;
		
		GLFWErrorCallback.createPrint(System.err).set();
		if(!glfwInit()) throw new IllegalStateException("GLFW failed to init");
		
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_RESIZABLE, (cfg.isResizable()) ? GLFW_TRUE:GLFW_FALSE);
		glfwWindowHint(GLFW_DECORATED, (cfg.isBordered()) ? GLFW_TRUE:GLFW_FALSE);
		glfwWindowHint(GLFW_SAMPLES, cfg.getSamples());
		
		window = glfwCreateWindow(cfg.getWidth(), cfg.getHeight(), cfg.getTitle(), (cfg.isFullscreen()) ? glfwGetPrimaryMonitor():0, 0);
		if(window == 0) throw new IllegalStateException("FAILED TO CREATE GLFW WINDOW");
		
		glfwSetKeyCallback(window, new Input());
		glfwMakeContextCurrent(window);
		glfwSwapInterval(1);
		glfwShowWindow(window);
		
		GL.createCapabilities();
		GL11.glViewport(0, 0, cfg.getWidth(), cfg.getHeight());
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	public static void render() {
		glfwSwapBuffers(window);
	}
	
	public static void update() {
		Mouse.update();
		Input.update();
		
		glfwPollEvents();
	}
	
	public static boolean isOpen() {
		return !glfwWindowShouldClose(window);
	}
	
	public static void destroy() {
		Callbacks.glfwFreeCallbacks(window);
		glfwDestroyWindow(window);
		glfwTerminate();
		glfwSetErrorCallback(null).free();
	}

	public static int getWidth() {
		return cfg.getWidth();
	}
	
	public static int getHeight() {
		return cfg.getHeight();
	}
	
	public static String getTitle() {
		return cfg.getTitle();
	}
	
	public static float getDesiredFps() {
		return cfg.getDesieredFps();
	}
}
