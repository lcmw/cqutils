package com.lcmw.cqe.util;

import static org.lwjgl.glfw.GLFW.*;

public class CQEngine {
	public static void begin(CQMainApp app) {
		double lastTime = glfwGetTime();
		double deltaTime = 0, nowTime = 0;
		
		app.load();		
		while(Display.isOpen()) {
			nowTime = glfwGetTime();
			deltaTime = (nowTime - lastTime);
			
			app.update((float) deltaTime);
			app.render();
			
			Display.update();
			Display.render();
			
			lastTime = nowTime;
		}
		
		app.exit();
		Delete.deleteAll();
		Display.destroy();
	
		System.exit(0);
	}
	
	public static void cleanExit(int code) {
		glfwSetWindowShouldClose(glfwGetCurrentContext(), true);
	}
}

