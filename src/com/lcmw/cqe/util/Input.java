package com.lcmw.cqe.util;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;

public class Input extends GLFWKeyCallback {
	
	private static Map<Integer, Boolean> keyMap = new HashMap<>(), prevKeyMap = new HashMap<>();

	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) {		
		keyMap.put(key, action != GLFW.GLFW_RELEASE);
	}
	
	public static void update() {
		for(Integer it:keyMap.keySet()) {
			prevKeyMap.put(it, keyMap.get(it));
		}
	}
	
	public static boolean isKeyDown(int keyCode) {
		if (keyMap.containsKey(keyCode)) {
			return keyMap.get(keyCode);
		} else {
			return false;
		}
	}

	public static boolean isKeyPressed(int keyCode) {
		if (isKeyDown(keyCode) && !wasKeyDown(keyCode)) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean wasKeyDown(int keyCode) {
		if (prevKeyMap.containsKey(keyCode)) {
			return prevKeyMap.get(keyCode);
		} else {
			return false;
		}
	}
}
