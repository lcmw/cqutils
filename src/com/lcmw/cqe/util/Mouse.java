package com.lcmw.cqe.util;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;
import static org.lwjgl.glfw.GLFW.glfwGetCurrentContext;
import static org.lwjgl.glfw.GLFW.glfwGetCursorPos;
import static org.lwjgl.glfw.GLFW.glfwGetMouseButton;

import java.nio.DoubleBuffer;
import java.util.HashMap;

import org.lwjgl.BufferUtils;

public class Mouse {
	private static HashMap<Integer, Boolean> isPressed = new HashMap<>();
	private static HashMap<Integer, Boolean>wasPressed = new HashMap<>();

	private static DoubleBuffer mouseBufferX = BufferUtils.createDoubleBuffer(1),
			mouseBufferY = BufferUtils.createDoubleBuffer(1);

	@SuppressWarnings("unchecked")
	public static void update() {
		wasPressed = (HashMap<Integer, Boolean>) isPressed.clone();
		glfwGetCursorPos(glfwGetCurrentContext(), mouseBufferX, mouseBufferY);
	}

	public static double getX() {
		double no = mouseBufferX.get();
		mouseBufferX.rewind();
		return no;
	}

	public static double getY() {
		double no = mouseBufferY.get();
		mouseBufferY.rewind();
		return no;
	}

	public static boolean isButtonClicked(int button) {
		if (isButtonDown(button) && !wasPressed.get(button)) {
			return true;
		}

		return false;
	}

	public static boolean isButtonDown(int button) {
		int result = glfwGetMouseButton(glfwGetCurrentContext(), button);
		if (result == GLFW_PRESS) {
			isPressed.put(button, true);
			return true;
		}

		isPressed.put(button, false);
		return false;
	}
	
}
